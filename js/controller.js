var raceResume = angular.module('raceResume', []);

raceResume.controller('raceCtrl', function($scope){
	$scope.races2015 = [
		{date: 'April 3', title: 'Good Friday Road Race - S4 - 63.6km', place: 27, started: 65, time: '1:44:07', speed: '36.7 kmh'},
		{date: 'April 19', title: 'Calabogie Road Classic - S4 - 55.55km', place: 15, started: 46, time: '1:22:14', speed: '40.5 kmh'},
		{date: 'April 28', title: 'NECC Time Trial - Bradford 15k', place: 1, started: 17, time: '23:30', speed: '38.2 kmh'},
	];
});